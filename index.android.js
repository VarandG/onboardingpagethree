/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  Button
} from 'react-native';

import Items from './android/app/src/component/items';
import GetItemsArray from './android/app/src/component/getItemsArray';


export default class OnBoarding extends Component {


  render() {



    return (
      <View style={{alignItems: 'center' }}>
      <View style={{alignItems: 'center', paddingLeft: 20, paddingRight: 20,marginBottom: 20}}>
        <Image
          style={{width: 290, height: 80,marginBottom: 25}}
          source={{uri: 'http://4.bp.blogspot.com/-TnM4fS2Js8A/VT4qEqMxrlI/AAAAAAAAH4A/tevg0jcZB5Q/s1600/helpin_logo.png'}}
        />
        <Text style={{fontSize: 18, fontWeight: 'bold'}}>Please  select  subjects  you are  intested in.</Text>
      </View>


      <View>
        <GetItemsArray/>
      </View>
      <View>
      <Image
        style={{width: 80, height: 40}}
        source={{uri: 'https://d30y9cdsu7xlg0.cloudfront.net/png/41421-200.png'}}
      />
      </View>

      <View style={{alignItems: 'center', flexDirection: 'row'}}>


      <View style={{width: 140, height: 70}}>
      <Button style={{width: 160, height: 80}}
        title="Go to App"
        color="#841584"
        />
      </View>

      <View style={{width: 140, height: 70, marginLeft: 10}}>
        <Button style={{width: 160, height: 80}}
          title="Finish Account"
          color="#841584"
          />
      </View>

      </View>

      </View>
    );
  }
}



AppRegistry.registerComponent('OnBoarding', () => OnBoarding);
