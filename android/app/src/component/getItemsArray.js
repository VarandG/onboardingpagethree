import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ListView
} from 'react-native';

import Items from './items';

export default class GetItemsArray extends Component {


  render() {
    var obj1 = new Items(require('../imgs/Food.png'));
    var obj2 = new Items(require('../imgs/Beauty.png'));
    var obj3 = new Items(require('../imgs/Drinks.png'));
    var obj4 = new Items(require('../imgs/Hotels.png'));
    var obj5 = new Items(require('../imgs/Movies.png'));
    var obj6 = new Items(require('../imgs/Music.png'));
    var obj7 = new Items(require('../imgs/Cars.png'));
    var obj8 = new Items(require('../imgs/Shopping.png'));
    var obj9 = new Items(require('../imgs/Food.png'));
    var data = [obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9];


    var m_columnCount = 3;
    var currentColumn = -1;
    var componentRow = [];
    var finalResult = [];
    for (var i = 0; i < data.length; i++) {
      currentColumn++;
      if (currentColumn == m_columnCount) {
        var row = <View style={{alignItems: 'center', flexDirection: 'row'}}>{componentRow}</View>
        finalResult.push(row);
        componentRow = [];
        currentColumn = 0;
      }
        var comp = <Image style={{width: 100, height: 100, margin: 3}} source={data[i].getUrl()} />;
        componentRow.push(comp);
    }
    var row = <View style={{alignItems: 'center', flexDirection: 'row'}}>{componentRow}</View>
    finalResult.push(row);

    return (
      <View>
        {finalResult}
      </View>
    );
  }
}

module.export = GetItemsArray;
