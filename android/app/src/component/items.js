import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

class Items {

  constructor(imgUrl)
  {
    this.url = imgUrl;
  }

  getUrl()
  {
    return this.url;
  }

}

export default Items;
